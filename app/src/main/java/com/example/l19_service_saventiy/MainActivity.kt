package com.example.l19_service_saventiy

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var logService = LogService()
    var isBound = false

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        read_logs_btn.setOnClickListener {
            if (isBound) {
                log_text_view.text = logService.read()
            }
        }

        read_logs_btn.setOnLongClickListener {
            if (isBound) {
                logService.clear()
                log_text_view.text = logService.read()
            }

            true
        }
    }

    override fun onStart() {
        super.onStart()
        val intent = Intent(baseContext, LogService::class.java)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }


    override fun onStop() {
        super.onStop()
        if (isBound) {
            unbindService(connection)
//            Toast.makeText(baseContext, "onStop", Toast.LENGTH_SHORT).show()
        }
    }


    private val connection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            isBound = false
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as LogService.LocalBinder
            logService = binder.getService()
            isBound = true
        }

    }


    fun saveButtonLog(v: View) {
        var b = v as Button
        if (isBound) {
            logService.write(b.text as String)
        }

    }


}
