package com.example.l19_service_saventiy

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Environment
import android.os.IBinder
import android.widget.Toast
import java.io.File

class LogService : Service() {

    private val binder = LocalBinder()
    private val filePath by lazy{ getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)}

    override fun onCreate() {
//        Toast.makeText(baseContext, "OnCreate", Toast.LENGTH_SHORT).show()
        super.onCreate()
    }

    override fun onBind(intent: Intent?): IBinder? {
//        Toast.makeText(baseContext, "onBind", Toast.LENGTH_SHORT).show()
        return binder
    }


    override fun onUnbind(intent: Intent?): Boolean {
//        Toast.makeText(baseContext, "onUnbind", Toast.LENGTH_SHORT).show()
        return super.onUnbind(intent)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(baseContext, "onStartCommand", Toast.LENGTH_SHORT).show()
        return START_NOT_STICKY
    }

    override fun onDestroy() {
//        Toast.makeText(baseContext, "onDestroy", Toast.LENGTH_SHORT).show()
        super.onDestroy()
    }

     inner class LocalBinder : Binder(){
         fun getService(): LogService = this@LogService
    }


    fun write(text: String){
        File(filePath, "data.txt").appendText(text + "\n")
    }

    fun read() : String{
        return  File(filePath, "data.txt").readText()
    }

    fun clear(){
        File(filePath, "data.txt").writeText("")
    }


}